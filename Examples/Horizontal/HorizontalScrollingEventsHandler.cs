using System.Collections;
using UnityEngine;
using Tacticsoft;

namespace Tacticsoft.Examples
{
	/// <summary>
	/// Horizontal scrolling events handler.
	/// </summary>
    public class HorizontalScrollingEventsHandler : MonoBehaviour
    {
        public HorizontalTableView m_tableView;

		/// <summary>
		/// Scrolls to top animated.
		/// </summary>
        public void ScrollToTopAnimated()
		{
            StartCoroutine(AnimateToScrollX(0, 2f));
        }

		/// <summary>
		/// Scrolls to bottom immediate.
		/// </summary>
		public void ScrollToBottomImmediate()
		{
			m_tableView.scrollX = m_tableView.scrollableWidth;
		}

		/// <summary>
		/// Scrolls to row10 animated.
		/// </summary>
        public void ScrollToRow10Animated() 
		{
            float scrollX = m_tableView.GetScrollXForIndex(10, true);
            StartCoroutine(AnimateToScrollX(scrollX, 2f));
        }

        //In practice, it is better to use libraries such as iTween to animate TableView's scrollX
        //This example uses a hard coded animator to keep dependencies down
        private IEnumerator AnimateToScrollX(float scrollX, float time) 
		{
            float startTime = Time.time;
            float startScrollX = m_tableView.scrollX;
            float endTime = startTime + time;
            while (Time.time < endTime) {
                float relativeProgress = Mathf.InverseLerp(startTime, endTime, Time.time);
                m_tableView.scrollX = Mathf.Lerp(startScrollX, scrollX, relativeProgress);
                yield return new WaitForEndOfFrame();
            }
            m_tableView.scrollX = scrollX;
        }
    }
}
