﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;

namespace Tacticsoft
{
	/// <summary>
	/// A reusable table for for (horizontal) tables. API inspired by Cocoa's UITableView
	/// Hierarchy structure should be :
	/// GameObject + TableView (this) + Mask + Scroll Rect (point to child)
	/// - Child GameObject + Vertical Layout Group
	/// This class should be after Unity's internal UI components in the Script Execution Order
	/// </summary>
	public class HorizontalTableView : TableViewBase
	{
		
		#region Public API
		
		/// <summary>
		/// Get or set the current scrolling position of the table
		/// </summary>
		public float scrollX
		{
			get {
				return m_scrollX;
			}
			set {
				if (this.isEmpty) {
					return;
				}

				value = Mathf.Clamp(value, 0f, GetScrollXForIndex(m_cellSizes.Length - 1, true));
				if (m_scrollX != value) {
					m_scrollX = value;
					m_requiresRefresh = true;
					float relativeScroll = value / this.scrollableWidth;
					m_scrollRect.horizontalNormalizedPosition = relativeScroll;
				}
			}
		}
		
		/// <summary>
		/// Get the x that the table would need to scroll to to have a certain column at the top
		/// </summary>
		/// <param name="column">The desired column</param>
		/// <param name="above">Should the top of the table be above the column or below the column?</param>
		/// <returns>The x position to scroll to, can be used with scrollX property</returns>
		public float GetScrollXForIndex(int index, bool above)
		{
			float retVal = CalculateContentWidthToIndex(index);
			if (above) {
				retVal -= m_cellSizes[index].x;
			}
			return retVal;
		}
		
		#endregion
		
		#region Private implementation

		private LayoutElement m_leftScrollRectDynamicFiller;
		private LayoutElement m_rightScrollRectDynamicFiller;
		
		/// <summary>
		/// Awake this instance.
		/// </summary>
		protected override void Awake()
		{
			base.Awake();
			m_leftScrollRectDynamicFiller = CreateEmptyScrollRectDynamicFillerElement("LeftScrollRectDynamicFiller");
			m_leftScrollRectDynamicFiller.transform.SetParent(m_scrollRect.content, false);
			m_rightScrollRectDynamicFiller = CreateEmptyScrollRectDynamicFillerElement("RightScrollRectDynamicFiller");
			m_rightScrollRectDynamicFiller.transform.SetParent(m_scrollRect.content, false);
		}

		/// <summary>
		/// Updates the padding elements.
		/// </summary>
		protected override void UpdateScrollRectDynamicFillerElements()
		{
			float hiddenElementsWidthSum = 0;
			for (int i = 0; i < m_visibleCellRange.from; i++) {
				hiddenElementsWidthSum += m_cellSizes[i].x;
			}
			m_leftScrollRectDynamicFiller.preferredWidth = hiddenElementsWidthSum;


			for (int i = m_visibleCellRange.from; i <= m_visibleCellRange.Last(); i++) {
				hiddenElementsWidthSum += m_cellSizes[i].x;
			}
			float bottomScrollRectDynamicFillerWidth = m_scrollRect.content.rect.width - hiddenElementsWidthSum;
			m_rightScrollRectDynamicFiller.preferredWidth = bottomScrollRectDynamicFillerWidth;
		}

		/// <summary>
		/// Calculates the current visible cell range.
		/// </summary>
		/// <returns>The current visible cell range.</returns>
		protected override Range CalculateCurrentVisibleCellRange()
		{
			float startX = m_scrollX;
			float endX = m_scrollX + (this.transform as RectTransform).rect.width;
			int startIndex = FindIndexOfColumnAtX(startX);
			int endIndex = FindIndexOfColumnAtX(endX);
			return new Range(startIndex, endIndex - startIndex + 1);
		}

		/// <summary>
		/// Gets the width of the cumulative cell.
		/// </summary>
		/// <returns>The cumulative cell width.</returns>
		/// <param name="index">Index.</param>
		protected override float CalculateContentWidthToIndex(int index)
		{
			// Only calculate cell cumulative widths of cells that come after any changed cell
			// Always run once on first pass
			float previousCellWidth = 0;
			while (m_cleanCumulativeIndex < index) {
				m_cleanCumulativeIndex++;
				m_cumulativeCellWidths[m_cleanCumulativeIndex] = m_cellSizes[m_cleanCumulativeIndex].x + previousCellWidth;
				previousCellWidth = m_cumulativeCellWidths[m_cleanCumulativeIndex];
			}
			return m_cumulativeCellWidths[index];
		}

		/// <summary>
		/// Calculates the index of the content height to.
		/// </summary>
		/// <returns>The content height to index.</returns>
		/// <param name="index">Index.</param>
		protected override float CalculateContentHeightToIndex(int index)
		{
			return this.TableContentHeight;
		}

		/// <summary>
		/// Finds the index of column at x.
		/// </summary>
		/// <returns>The index of column at x.</returns>
		/// <param name="x">The x coordinate.</param>
		private int FindIndexOfColumnAtX(float x)
		{
			//TODO : Binary search if inside clean cumulative column width area, else walk until found.
			return FindIndexOfColumnAtX(x, 0, m_cumulativeCellWidths.Length - 1);
		}

		/// <summary>
		/// Finds the index of column at x.
		/// </summary>
		/// <returns>The index of column at x.</returns>
		/// <param name="x">The x coordinate.</param>
		/// <param name="startIndex">Start index.</param>
		/// <param name="endIndex">End index.</param>
		private int FindIndexOfColumnAtX(float x, int startIndex, int endIndex)
		{
			if (startIndex >= endIndex) {
				return startIndex;
			}

			int midIndex = (startIndex + endIndex) / 2;
			if (CalculateContentWidthToIndex(midIndex) >= x) {
				return FindIndexOfColumnAtX(x, startIndex, midIndex);
			} else {
				return FindIndexOfColumnAtX(x, midIndex + 1, endIndex);
			}
		}
		
		#endregion
	}
}
