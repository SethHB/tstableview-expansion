using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;

namespace Tacticsoft
{
    /// <summary>
    /// A reusable table for for (vertical) tables. API inspired by Cocoa's UITableView
    /// Hierarchy structure should be :
    /// GameObject + TableView (this) + Mask + Scroll Rect (point to child)
    /// - Child GameObject + Vertical Layout Group
    /// This class should be after Unity's internal UI components in the Script Execution Order
    /// </summary>
    public class VerticalTableView : TableViewBase
    {

        #region Public API

		/// <summary>
        /// Get or set the current scrolling position of the table
        /// </summary>
        public float scrollY
		{
            get {
                return m_scrollY;
            }
            set {
                if (this.isEmpty) {
                    return;
                }
                value = Mathf.Clamp(value, 0, GetScrollYForRow(m_cellSizes.Length - 1, true));
                if (m_scrollY != value) {
                    m_scrollY = value;
                    m_requiresRefresh = true;
                    float relativeScroll = value / this.scrollableHeight;
                    m_scrollRect.verticalNormalizedPosition = 1 - relativeScroll;
                }
            }
        }

        /// <summary>
        /// Get the y that the table would need to scroll to to have a certain row at the top
        /// </summary>
        /// <param name="row">The desired row</param>
        /// <param name="above">Should the top of the table be above the row or below the row?</param>
        /// <returns>The y position to scroll to, can be used with scrollY property</returns>
        public float GetScrollYForRow(int row, bool above)
		{
			float retVal = CalculateContentHeightToIndex(row);
            if (above) {
                retVal -= m_cellSizes[row].y;
            }
            return retVal;
        }

        #endregion

        #region Private implementation

        private LayoutElement m_topScrollRectDynamicFiller;
        private LayoutElement m_bottomScrollRectDynamicFiller;

		/// <summary>
		/// Awake this instance.
		/// </summary>
		protected override void Awake()
        {
			base.Awake();
            m_topScrollRectDynamicFiller = CreateEmptyScrollRectDynamicFillerElement("TopScrollRectDynamicFiller");
            m_topScrollRectDynamicFiller.transform.SetParent(m_scrollRect.content, false);
            m_bottomScrollRectDynamicFiller = CreateEmptyScrollRectDynamicFillerElement("BottomScrollRectDynamicFiller");
            m_bottomScrollRectDynamicFiller.transform.SetParent(m_scrollRect.content, false);
        }

		/// <summary>
		/// Updates the padding elements.
		/// </summary>
		protected override void UpdateScrollRectDynamicFillerElements()
		{
            float hiddenElementsHeightSum = 0;
            for (int i = 0; i < m_visibleCellRange.from; i++) {
				hiddenElementsHeightSum += m_cellSizes[i].y;
            }
            m_topScrollRectDynamicFiller.preferredHeight = hiddenElementsHeightSum;
            for (int i = m_visibleCellRange.from; i <= m_visibleCellRange.Last(); i++) {
				hiddenElementsHeightSum += m_cellSizes[i].y;
            }
            float bottomScrollRectDynamicFillerHeight = m_scrollRect.content.rect.height - hiddenElementsHeightSum;
            m_bottomScrollRectDynamicFiller.preferredHeight = bottomScrollRectDynamicFillerHeight;
        }

		/// <summary>
		/// Calculates the current visible cell range.
		/// </summary>
		/// <returns>The current visible cell range.</returns>
		protected override Range CalculateCurrentVisibleCellRange()
		{
			float startY = m_scrollY;
			float endY = m_scrollY + (this.transform as RectTransform).rect.height;
			int startIndex = FindIndexOfRowAtY(startY);
			int endIndex = FindIndexOfRowAtY(endY);
			return new Range(startIndex, endIndex - startIndex + 1);
		}

		/// <summary>
		/// Calculates the index of the content height to.
		/// </summary>
		/// <returns>The content height to index.</returns>
		/// <param name="index">Index.</param>
		protected override float CalculateContentHeightToIndex(int index)
		{
			// Only calculate cell cumulative widths of cells that come after any changed cell
			// Always run once on first pass
			float previousCellHeight = 0;
			while (m_cleanCumulativeIndex < index) {
				m_cleanCumulativeIndex++;
				m_cumulativeCellHeights[m_cleanCumulativeIndex] = m_cellSizes[m_cleanCumulativeIndex].y + previousCellHeight;
				previousCellHeight = m_cumulativeCellHeights[m_cleanCumulativeIndex];
			}
			return m_cumulativeCellHeights[index];
		}

		/// <summary>
		/// Calculates the index of the content width to.
		/// </summary>
		/// <returns>The content width to index.</returns>
		/// <param name="index">Index.</param>
		protected override float CalculateContentWidthToIndex(int index)
		{
			return this.TableContentWidth;
		}

		/// <summary>
		/// Finds the index of row at y.
		/// </summary>
		/// <returns>The index of row at y.</returns>
		/// <param name="y">The y coordinate.</param>
        private int FindIndexOfRowAtY(float y)
		{
            //TODO : Binary search if inside clean cumulative row height area, else walk until found.
            return FindIndexOfRowAtY(y, 0, m_cumulativeCellHeights.Length - 1);
        }

		/// <summary>
		/// Finds the index of row at y.
		/// </summary>
		/// <returns>The index of row at y.</returns>
		/// <param name="y">The y coordinate.</param>
		/// <param name="startIndex">Start index.</param>
		/// <param name="endIndex">End index.</param>
        private int FindIndexOfRowAtY(float y, int startIndex, int endIndex)
		{
            if (startIndex >= endIndex) {
                return startIndex;
            }
            int midIndex = (startIndex + endIndex) / 2;
			if (CalculateContentHeightToIndex(midIndex) >= y) {
                return FindIndexOfRowAtY(y, startIndex, midIndex);
            } else {
                return FindIndexOfRowAtY(y, midIndex + 1, endIndex);
            }
        }

        #endregion
    }
}
